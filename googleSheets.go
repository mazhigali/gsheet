//https://developers.google.com/sheets/api/quickstart/go?hl=ru
package gsheet

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/sheets/v4"
)

type GsheetConnection struct {
	//Client        *http.Client
	Service       *sheets.Service
	SpreadsheetId string
}

func NewGsheetConnection(spreadsheetId string) (conn *GsheetConnection) {
	conn = new(GsheetConnection)
	conn.Service = getService()
	conn.SpreadsheetId = spreadsheetId
	return
}

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func getService() *sheets.Service {
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets")
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := getClient(config)

	srv, err := sheets.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Sheets client: %v", err)
	}
	return srv
}

//TODO Read cells from range
func (con *GsheetConnection) ReadCells() {
	//readRange := "Class Data!A2:E"
	readRange := "Москва!A2:E"
	resp, err := con.Service.Spreadsheets.Values.Get(con.SpreadsheetId, readRange).Do()
	if err != nil {
		log.Fatalf("Unable to retrieve data from sheet: %v", err)
	}

	if len(resp.Values) == 0 {
		fmt.Println("No data found.")
	} else {
		fmt.Println("Name, Major:")
		for _, row := range resp.Values {
			// Print columns A and E, which correspond to indices 0 and 4.
			fmt.Printf("%s, %s\n", row[0], row[1])
			fmt.Println(row)
		}
	}

}

func (con *GsheetConnection) AppendRow(writeRange string, data [][]interface{}) error {
	// The A1 notation of a range to search for a logical table of data.
	// Values will be appended after the last row of the table.
	//range2 := "Москва!A2:E"

	rb := &sheets.ValueRange{
		MajorDimension: "ROWS",
		Values:         data,
	}

	resp, err := con.Service.Spreadsheets.Values.Append(con.SpreadsheetId, writeRange, rb).ValueInputOption("USER_ENTERED").InsertDataOption("INSERT_ROWS").Do()
	if err != nil {
		return fmt.Errorf("Unable to append data to sheet: %v", err)
	}

	//fmt.Printf("%#v\n", resp)
	if resp.ServerResponse.HTTPStatusCode != 200 {
		return fmt.Errorf("gAPI HTTPstatus error: %v", resp.ServerResponse.HTTPStatusCode)
	}
	return nil
}

// ReadSheetsTitles extracts the titles of the sheets
func (con *GsheetConnection) GetSheetTitles() ([]string, error) {
	call := con.Service.Spreadsheets.Get(con.SpreadsheetId)

	resp, err := call.Fields("sheets.properties").Do()
	if err != nil {
		return nil, err
	}

	titles := make([]string, len(resp.Sheets))
	for i, s := range resp.Sheets {
		//fmt.Printf("- Title: %s\n", s.Properties.Title)
		//fmt.Printf("- ID: %v\n", s.Properties.SheetId)
		titles[i] = s.Properties.Title
	}
	return titles, nil
}

func (con *GsheetConnection) FindSheetId(titleSheet string) (int64, error) {
	var result int64
	call := con.Service.Spreadsheets.Get(con.SpreadsheetId)

	resp, err := call.Fields("sheets.properties").Do()
	if err != nil {
		return result, err
	}

	for _, s := range resp.Sheets {
		//fmt.Printf("- Title: %s\n", s.Properties.Title)
		//fmt.Printf("- ID: %v\n", s.Properties.SheetId)
		if s.Properties.Title == titleSheet {
			result = s.Properties.SheetId
		}
	}
	if result == 0 {
		return result, fmt.Errorf("Not found sheet titile: %v", titleSheet)

	}

	return result, nil
}

//add new sheet to spreadsheet
func (con *GsheetConnection) AddSheet(titleSheet string) error {
	if titleSheet == "" {
		return fmt.Errorf("Title is Empty: %v", titleSheet)
	}

	addsheet := sheets.AddSheetRequest{Properties: &sheets.SheetProperties{Title: titleSheet}}
	bur := sheets.BatchUpdateSpreadsheetRequest{
		Requests: []*sheets.Request{
			&sheets.Request{
				AddSheet: &addsheet,
			}}}

	res, err := con.Service.Spreadsheets.BatchUpdate(
		con.SpreadsheetId,
		&bur,
	).Do()
	if err != nil {
		return fmt.Errorf("Unable to add sheet: %v", err)
	}

	if res.HTTPStatusCode != 200 {
		return fmt.Errorf("gAPI HTTPstatus error: %v", res.HTTPStatusCode)
	}
	//fmt.Println(res)
	return err
}

//make copy of existing sheet
func (con *GsheetConnection) DuplicateSheet(srcSheetID int64, newSheetName string) error {
	if newSheetName == "" {
		return fmt.Errorf("New title is Empty: %v", newSheetName)
	}

	bur := sheets.BatchUpdateSpreadsheetRequest{
		Requests: []*sheets.Request{
			{
				DuplicateSheet: &sheets.DuplicateSheetRequest{
					NewSheetName:  newSheetName,
					SourceSheetId: srcSheetID,
					//NewSheetId:    newSheetID,
				},
			},
		},
	}

	res, err := con.Service.Spreadsheets.BatchUpdate(
		con.SpreadsheetId,
		&bur,
	).Do()
	if err != nil {
		return fmt.Errorf("Unable to duplicate sheet: %v", err)
	}

	if res.HTTPStatusCode != 200 {
		return fmt.Errorf("gAPI duplicate HTTPstatus error: %v", res.HTTPStatusCode)
	}
	//fmt.Println(res)
	return err
}
